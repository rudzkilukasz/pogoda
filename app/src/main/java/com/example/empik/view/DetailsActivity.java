package com.example.empik.view;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;

import com.example.empik.R;
import com.example.empik.databinding.ActivityDetailsBinding;
import com.example.empik.model.repository.DetailRepository;
import com.example.empik.view.adapter.DailyForecastsAdapter;
import com.example.empik.view.base.BaseActivity;
import com.example.empik.viewmodel.DetailsViewModel;

import javax.inject.Inject;

public class DetailsActivity extends BaseActivity {

    public static final String DetailsActivityBundle = "DetailsActivityBundle";
    ActivityDetailsBinding binding;

    @Inject
    DailyForecastsAdapter dailyForecastsAdapter;

    DetailsViewModel detailsViewModel;

    @Inject
    DetailRepository detailRepository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getActivityComponent().inject(this);
        detailsViewModel = ViewModelProviders.of(this, detailRepository).get(DetailsViewModel.class);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_details);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            String key = (String) getIntent().getExtras().get(DetailsActivityBundle);
            detailsViewModel.init(key);
            binding.setVm(detailsViewModel);
            binding.setLifecycleOwner(this);

            detailsViewModel.getDailyForecastsResponseMutableLiveData().observe(this, dailyForecastsResponse -> {
                dailyForecastsAdapter.setDailyForecastsList(dailyForecastsResponse.getDailyForecasts());
                dailyForecastsAdapter.notifyDataSetChanged();
            });
        }

        setupRecyclerView();
    }

    private void setupRecyclerView() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        binding.rvDailyforecasts.setLayoutManager(linearLayoutManager);
        binding.rvDailyforecasts.setAdapter(dailyForecastsAdapter);
    }
}

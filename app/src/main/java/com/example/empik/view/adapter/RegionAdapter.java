package com.example.empik.view.adapter;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.empik.BR;
import com.example.empik.R;
import com.example.empik.model.database.item.SearchItem;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class RegionAdapter extends RecyclerView.Adapter<RegionAdapter.RegionHolder> {
    private List<SearchItem> searchItemList = new ArrayList<>();
    private View.OnClickListener onClickListener;

    public List<SearchItem> getSearchItemList() {
        return searchItemList;
    }

    public void setSearchItemList(List<SearchItem> searchItemList) {
        this.searchItemList = searchItemList;
    }

    public void setOnClickListener(View.OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    @Inject
    public RegionAdapter() {
    }

    @NonNull
    @Override
    public RegionHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        ViewDataBinding viewDataBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.region_list_item, parent, false);
        return new RegionHolder(viewDataBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull RegionHolder holder, int i) {
        SearchItem searchItem = searchItemList.get(i);
        holder.getBinding().setVariable(BR.search, searchItem);
        holder.getBinding().executePendingBindings();
    }

    @Override
    public int getItemCount() {
        if(searchItemList == null)
            return 0;

        return searchItemList.size();
    }

    class RegionHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ViewDataBinding binding;

        public RegionHolder(ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            itemView.setOnClickListener(this);
        }

        public ViewDataBinding getBinding() {
            return binding;
        }

        @Override
        public void onClick(View v) {
            onClickListener.onClick(v);
        }
    }

}

package com.example.empik.view.adapter;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.empik.BR;
import com.example.empik.R;
import com.example.empik.model.api.response.DailyForecastsResponse;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class DailyForecastsAdapter extends RecyclerView.Adapter<DailyForecastsAdapter.DailyForecastHolder> {

    private List<DailyForecastsResponse.DailyForecasts> dailyForecastsList = new ArrayList<>();

    public void setDailyForecastsList(List<DailyForecastsResponse.DailyForecasts> dailyForecastsList) {
        this.dailyForecastsList = dailyForecastsList;
    }

    @Inject
    public DailyForecastsAdapter() {
    }

    @NonNull
    @Override
    public DailyForecastHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        ViewDataBinding viewDataBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.dailyforecasts_item, parent, false);
        return new DailyForecastHolder(viewDataBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DailyForecastHolder dailyForecastHolder, int i) {
        DailyForecastsResponse.DailyForecasts dailyForecasts = dailyForecastsList.get(i);
        dailyForecastHolder.getBinding().setVariable(BR.dailyforecasts, dailyForecasts);
        dailyForecastHolder.getBinding().executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return dailyForecastsList.size();
    }

    class DailyForecastHolder extends RecyclerView.ViewHolder {
        private ViewDataBinding binding;

        public DailyForecastHolder(ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public ViewDataBinding getBinding() {
            return binding;
        }
    }


}

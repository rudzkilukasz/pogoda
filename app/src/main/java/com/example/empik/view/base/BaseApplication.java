package com.example.empik.view.base;

import android.app.Activity;
import android.app.Application;

import com.example.empik.dependencyInjection.ApplicationComponent;
import com.example.empik.dependencyInjection.DaggerApplicationComponent;
import com.example.empik.dependencyInjection.modules.ContextModule;
import com.example.empik.dependencyInjection.modules.RoomModule;

public class BaseApplication extends Application {
    private ApplicationComponent applicationComponent;

    public static BaseApplication get(Activity activity) {
        return (BaseApplication) activity.getApplication();
    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }

    private void createComponent() {
        applicationComponent = DaggerApplicationComponent.builder()
                .contextModule(new ContextModule(this))
                .roomModule(new RoomModule(this))
                .build();
    }

    private void setupInject() {
        applicationComponent.inject(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        createComponent();
        setupInject();
    }
}

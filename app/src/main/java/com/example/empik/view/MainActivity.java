package com.example.empik.view;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.Toast;

import com.arlib.floatingsearchview.FloatingSearchView;
import com.arlib.floatingsearchview.suggestions.model.SearchSuggestion;
import com.example.empik.Navigator;
import com.example.empik.R;
import com.example.empik.databinding.ActivityMainBinding;
import com.example.empik.model.api.response.AutocompleteSearchResponse;
import com.example.empik.model.database.item.SearchItem;
import com.example.empik.model.repository.MainRepository;
import com.example.empik.utils.RxDebounceOnQueryChangeListener;
import com.example.empik.view.adapter.RegionAdapter;
import com.example.empik.view.base.BaseActivity;
import com.example.empik.viewmodel.MainViewModel;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.observers.DisposableObserver;

public class MainActivity extends BaseActivity {

    ActivityMainBinding binding;

    @Inject
    MainRepository mainRepository;
    MainViewModel mainViewModel;

    @Inject
    RegionAdapter regionAdapter;

    @Inject
    Navigator navigator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getActivityComponent().inject(MainActivity.this);
        mainViewModel = ViewModelProviders.of(this, mainRepository).get(MainViewModel.class);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        setupRecyclerView();
        setupFloatingSearch();
    }

    private void setupFloatingSearch() {
        FloatingSearchView mSearchView = binding.floatingSearchView;

        RxDebounceOnQueryChangeListener rxDebounceOnQueryChangeListener = new RxDebounceOnQueryChangeListener(1000, (oldQuery, newQuery) -> {
            if (!oldQuery.equals("") && newQuery.equals("")) {
                mSearchView.clearSuggestions();
            } else {
                mSearchView.showProgress();
            }
        }, new RxDebounceOnQueryChangeListener.OnQueryDebounceListener() {
            @Override
            public void onSearchTextDebounce(String query) {
                mainRepository.getGetAutocompleteSearchUC().execute(new DisposableObserver<List<AutocompleteSearchResponse>>() {
                    List<AutocompleteSearchResponse> autocompleteSearchResponses;

                    @Override
                    public void onNext(List<AutocompleteSearchResponse> autocompleteSearchResponses) {
                        this.autocompleteSearchResponses = autocompleteSearchResponses;
                    }

                    @Override
                    public void onError(Throwable e) {
                        Toast.makeText(MainActivity.this, getString(R.string.error_message) + e.getMessage(), Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onComplete() {
                        if (autocompleteSearchResponses.size() == 0)
                            Toast.makeText(MainActivity.this, R.string.no_result, Toast.LENGTH_LONG).show();

                        mSearchView.swapSuggestions(autocompleteSearchResponses);
                        mSearchView.hideProgress();
                    }
                }, query);
            }
        });
        mSearchView.setOnQueryChangeListener(rxDebounceOnQueryChangeListener);

        mSearchView.setOnSearchListener(new FloatingSearchView.OnSearchListener() {
            @Override
            public void onSuggestionClicked(final SearchSuggestion searchSuggestion) {
                if (!(searchSuggestion instanceof AutocompleteSearchResponse)) {
                    return;
                }

                AutocompleteSearchResponse autocompleteSearchResponse = (AutocompleteSearchResponse) searchSuggestion;
                navigator.navigateToDetailActivity(autocompleteSearchResponse.getKey());

                mainRepository.getInsertSearchItemUC().execute(new DisposableObserver<SearchItem>() {
                    @Override
                    public void onNext(SearchItem searchItem) {
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {
                    }
                }, autocompleteSearchResponse);

                mSearchView.clearFocus();
                mSearchView.clearQuery();
            }

            @Override
            public void onSearchAction(String query) {

            }
        });
    }

    private void setupRecyclerView() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        binding.rv.setLayoutManager(linearLayoutManager);
        binding.rv.setAdapter(regionAdapter);

        regionAdapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SearchItem searchItem = regionAdapter.getSearchItemList().get(binding.rv.getChildLayoutPosition(v));
                navigator.navigateToDetailActivity(searchItem.getLocalizeKey());
            }
        });

        mainViewModel.getListLiveDataSearchItem().observe(this, list -> {
            regionAdapter.setSearchItemList(list);
            regionAdapter.notifyDataSetChanged();
        });
    }
}

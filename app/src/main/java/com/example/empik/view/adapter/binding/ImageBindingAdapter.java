package com.example.empik.view.adapter.binding;

import android.databinding.BindingAdapter;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

public class ImageBindingAdapter {
    @BindingAdapter(value = {"bind:imageUrl"}, requireAll=false)
    public static void loadImage(final ImageView imageView, String url) {

        Glide.with(imageView.getContext())
                .load(url)
                .into(imageView);
    }

}

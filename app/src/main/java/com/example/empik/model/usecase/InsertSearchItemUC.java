package com.example.empik.model.usecase;

import com.example.empik.model.api.response.AutocompleteSearchResponse;
import com.example.empik.model.database.MyDatabase;
import com.example.empik.model.database.item.SearchItem;

import java.util.concurrent.Callable;

import javax.inject.Inject;

import io.reactivex.Observable;

public class InsertSearchItemUC extends UseCase<SearchItem, AutocompleteSearchResponse> {

    @Inject
    MyDatabase myDatabase;

    @Inject
    public InsertSearchItemUC() {
    }

    @Override
    public Observable<SearchItem> buildUseCaseObservable(AutocompleteSearchResponse autocompleteSearchResponse) {
        SearchItem searchItem = new SearchItem();
        searchItem.setLocalizedName(autocompleteSearchResponse.getLocalizedName());
        searchItem.setAdministrativeArea(autocompleteSearchResponse.getAdministrativeArea().getID());
        searchItem.setCountry(autocompleteSearchResponse.getCountry().getID());
        searchItem.setLocalizeKey(autocompleteSearchResponse.getKey());

        Observable<SearchItem> observable = Observable.fromCallable(new Callable<SearchItem>() {
            @Override
            public SearchItem call() throws Exception {
                myDatabase.searchDao().insertItem(searchItem);
                return searchItem;
            }
        });

        return observable;
    }


}

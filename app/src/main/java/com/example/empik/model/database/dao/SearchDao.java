package com.example.empik.model.database.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.example.empik.model.database.item.SearchItem;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

@Dao
public interface SearchDao {

    @Delete
    void deleteItem(SearchItem item);

    @Query("DELETE FROM SearchItem")
    void deleteAllItems();

    @Query("SELECT * FROM SearchItem")
    LiveData<List<SearchItem>> selectAllItems();

    @Query("SELECT * FROM SearchItem")
    List<SearchItem> selectAllItemsNormal();

    @Query("SELECT * FROM SearchItem WHERE id = :id")
    SearchItem getItemById(long id);

    @Query("SELECT * FROM SearchItem WHERE id = :id")
    LiveData<SearchItem> getItemByIdLiveData(long id);

    @Insert(onConflict = REPLACE)
    Long insertItem(SearchItem item);

}

package com.example.empik.model.repository;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.content.Context;
import android.support.annotation.NonNull;

import com.example.empik.model.usecase.GetDailyForecastsUC;
import com.example.empik.viewmodel.DetailsViewModel;
import com.example.empik.viewmodel.MainViewModel;

import javax.inject.Inject;

public class DetailRepository implements ViewModelProvider.Factory {

    @Inject
    public DetailRepository() {
    }

    @Inject
    Context context;

    public Context getContext() {
        return context;
    }

    @Inject
    GetDailyForecastsUC getDailyForecastsUC;

    public GetDailyForecastsUC getGetDailyForecastsUC() {
        return getDailyForecastsUC;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new DetailsViewModel(this);
    }
}

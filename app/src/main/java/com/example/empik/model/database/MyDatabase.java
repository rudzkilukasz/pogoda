package com.example.empik.model.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.example.empik.model.database.dao.SearchDao;
import com.example.empik.model.database.item.SearchItem;

@Database(entities = {SearchItem.class}, version = 1, exportSchema = false)
public abstract class MyDatabase extends RoomDatabase {
    public abstract SearchDao searchDao();
}

package com.example.empik.model.api;

import com.example.empik.model.api.response.AutocompleteSearchResponse;
import com.example.empik.model.api.response.DailyForecastsResponse;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ModelClient {

    @GET("locations/v1/cities/autocomplete")
    Observable<List<AutocompleteSearchResponse>> getAutocompleteSearchResponse(@Query("q") String textToSearch);

    @GET("forecasts/v1/daily/5day/{iDkey}")
    Observable<DailyForecastsResponse> getDailyForecastsResponse(@Path("iDkey") String key);
}

package com.example.empik.model.usecase;

import com.example.empik.model.api.ModelClient;
import com.example.empik.model.api.response.DailyForecastsResponse;

import javax.inject.Inject;

import io.reactivex.Observable;


public class GetDailyForecastsUC extends UseCase<DailyForecastsResponse, String> {

    @Inject
    ModelClient modelClient;

    @Inject
    public GetDailyForecastsUC() {
    }

    @Override
    public Observable<DailyForecastsResponse> buildUseCaseObservable(String s) {
        return modelClient.getDailyForecastsResponse(s);
    }
}

package com.example.empik.model.api.response;

import android.graphics.Color;

import java.util.List;

public class DailyForecastsResponse {

    List<DailyForecasts> DailyForecasts;
    Headline Headline;

    public DailyForecastsResponse(List<DailyForecastsResponse.DailyForecasts> dailyForecasts, DailyForecastsResponse.Headline headline) {
        DailyForecasts = dailyForecasts;
        Headline = headline;
    }

    public List<DailyForecastsResponse.DailyForecasts> getDailyForecasts() {
        return DailyForecasts;
    }

    public void setDailyForecasts(List<DailyForecastsResponse.DailyForecasts> dailyForecasts) {
        DailyForecasts = dailyForecasts;
    }

    public DailyForecastsResponse.Headline getHeadline() {
        return Headline;
    }

    public void setHeadline(DailyForecastsResponse.Headline headline) {
        Headline = headline;
    }

    public class DailyForecasts {
        private String Date;
        private long EpochDate;
        private Temperature Temperature;
        private DayNight Day;
        private DayNight Night;

        public DailyForecasts(String date, long epochDate, DailyForecastsResponse.Temperature temperature, DayNight day, DayNight night) {
            Date = date;
            EpochDate = epochDate;
            Temperature = temperature;
            Day = day;
            Night = night;
        }

        public String getDate() {
            return Date.substring(0,10);
        }

        private int fahrenheitToCelsius(int value) {
            return (value - 32) * 5 / 9;
        }

        public int getMinimumTemperature() {
            int temperatureFahrenheit = Temperature.Minimum.getValue();
            int tempCelsius = fahrenheitToCelsius(temperatureFahrenheit);
            return tempCelsius;
        }

        public int getMaximumTemperature() {
            int temperatureFahrenheit = Temperature.Maximum.getValue();
            int tempCelsius = fahrenheitToCelsius(temperatureFahrenheit);
            return tempCelsius;
        }

        private int getColorByTemperature(int temperature) {
            if (temperature < 10)
                return Color.BLUE;

            if (temperature > 20)
                return Color.RED;

            return Color.BLACK;
        }

        public int getMinimumTemperatureColor() {
            int minTemperature = getMinimumTemperature();
            return getColorByTemperature(minTemperature);
        }

        public int getMaximumTemperatureColor() {
            int maxTemperature = getMaximumTemperature();
            return getColorByTemperature(maxTemperature);
        }

        public DailyForecastsResponse.Temperature getTemperature() {
            return Temperature;
        }

        public void setTemperature(DailyForecastsResponse.Temperature temperature) {
            Temperature = temperature;
        }


        public void setDate(String date) {
            Date = date;
        }

        public long getEpochDate() {
            return EpochDate;
        }

        public void setEpochDate(long epochDate) {
            EpochDate = epochDate;
        }

        public DayNight getDay() {
            return Day;
        }

        public void setDay(DayNight day) {
            Day = day;
        }

        public DayNight getNight() {
            return Night;
        }

        public void setNight(DayNight night) {
            Night = night;
        }
    }

    public class Temperature {
        TemperatureMinMax Minimum;
        TemperatureMinMax Maximum;

        public Temperature(TemperatureMinMax minimum, TemperatureMinMax maximum) {
            Minimum = minimum;
            Maximum = maximum;
        }

        public TemperatureMinMax getMinimum() {
            return Minimum;
        }

        public void setMinimum(TemperatureMinMax minimum) {
            Minimum = minimum;
        }

        public TemperatureMinMax getMaximum() {
            return Maximum;
        }

        public void setMaximum(TemperatureMinMax maximum) {
            Maximum = maximum;
        }
    }

    public class TemperatureMinMax {
        private int Value;
        private String Unit;
        private int UnitType;

        public TemperatureMinMax(int value, String unit, int unitType) {
            Value = value;
            Unit = unit;
            UnitType = unitType;
        }

        public int getValue() {
            return Value;
        }

        public void setValue(int value) {
            Value = value;
        }

        public String getUnit() {
            return Unit;
        }

        public void setUnit(String unit) {
            Unit = unit;
        }

        public int getUnitType() {
            return UnitType;
        }

        public void setUnitType(int unitType) {
            UnitType = unitType;
        }
    }

    public class DayNight {
        private int Icon;
        private String IconPhrase;
        private boolean HasPrecipitation;
        private String PrecipitationType;
        private String PrecipitationIntensity;

        public DayNight(int icon, String iconPhrase, boolean hasPrecipitation, String precipitationType, String precipitationIntensity) {
            Icon = icon;
            IconPhrase = iconPhrase;
            HasPrecipitation = hasPrecipitation;
            PrecipitationType = precipitationType;
            PrecipitationIntensity = precipitationIntensity;
        }

        public int getIcon() {
            return Icon;
        }

        public void setIcon(int icon) {
            Icon = icon;
        }

        public String getIconPhrase() {
            return IconPhrase;
        }

        public void setIconPhrase(String iconPhrase) {
            IconPhrase = iconPhrase;
        }

        public boolean isHasPrecipitation() {
            return HasPrecipitation;
        }

        public void setHasPrecipitation(boolean hasPrecipitation) {
            HasPrecipitation = hasPrecipitation;
        }

        public String getPrecipitationType() {
            return PrecipitationType;
        }

        public void setPrecipitationType(String precipitationType) {
            PrecipitationType = precipitationType;
        }

        public String getPrecipitationIntensity() {
            return PrecipitationIntensity;
        }

        public void setPrecipitationIntensity(String precipitationIntensity) {
            PrecipitationIntensity = precipitationIntensity;
        }
    }

    public class Headline {
        private String EffectiveDate;
        private long EffectiveEpochDate;
        private int Severity;
        private String Text;
        private String Category;
        private String EndDate;
        private long EndEpochDate;

        public Headline(String effectiveDate, long effectiveEpochDate, int severity, String text, String category, String endDate, long endEpochDate) {
            EffectiveDate = effectiveDate;
            EffectiveEpochDate = effectiveEpochDate;
            Severity = severity;
            Text = text;
            Category = category;
            EndDate = endDate;
            EndEpochDate = endEpochDate;
        }

        public String getEffectiveDate() {
            return EffectiveDate;
        }

        public void setEffectiveDate(String effectiveDate) {
            EffectiveDate = effectiveDate;
        }

        public long getEffectiveEpochDate() {
            return EffectiveEpochDate;
        }

        public void setEffectiveEpochDate(long effectiveEpochDate) {
            EffectiveEpochDate = effectiveEpochDate;
        }

        public int getSeverity() {
            return Severity;
        }

        public void setSeverity(int severity) {
            Severity = severity;
        }

        public String getText() {
            return Text;
        }

        public void setText(String text) {
            Text = text;
        }

        public String getCategory() {
            return Category;
        }

        public void setCategory(String category) {
            Category = category;
        }

        public String getEndDate() {
            return EndDate;
        }

        public void setEndDate(String endDate) {
            EndDate = endDate;
        }

        public long getEndEpochDate() {
            return EndEpochDate;
        }

        public void setEndEpochDate(long endEpochDate) {
            EndEpochDate = endEpochDate;
        }
    }

}

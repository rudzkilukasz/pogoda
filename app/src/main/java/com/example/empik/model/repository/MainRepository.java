package com.example.empik.model.repository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import com.example.empik.model.database.MyDatabase;
import com.example.empik.model.database.item.SearchItem;
import com.example.empik.model.usecase.GetAutocompleteSearchUC;
import com.example.empik.model.usecase.InsertSearchItemUC;
import com.example.empik.viewmodel.MainViewModel;

import java.util.List;

import javax.inject.Inject;

public class MainRepository implements ViewModelProvider.Factory {

    @Inject
    public MainRepository() {
    }

    @Inject
    MyDatabase myDatabase;

    @Inject
    InsertSearchItemUC insertSearchItemUC;

    public InsertSearchItemUC getInsertSearchItemUC() {
        return insertSearchItemUC;
    }

    @Inject
    GetAutocompleteSearchUC getAutocompleteSearchUC;

    public GetAutocompleteSearchUC getGetAutocompleteSearchUC() {
        return getAutocompleteSearchUC;
    }

    public LiveData<List<SearchItem>> getSearchItem() {
        return myDatabase.searchDao().selectAllItems();
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new MainViewModel(this);
    }
}

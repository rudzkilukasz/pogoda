package com.example.empik.model.usecase;

import com.example.empik.model.api.ModelClient;
import com.example.empik.model.api.response.AutocompleteSearchResponse;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;

public class GetAutocompleteSearchUC extends UseCase<List<AutocompleteSearchResponse>, String> {

    @Inject
    ModelClient modelClient;

    @Inject
    public GetAutocompleteSearchUC() {
    }

    @Override
    public Observable<List<AutocompleteSearchResponse>> buildUseCaseObservable(String s) {
        return modelClient.getAutocompleteSearchResponse(s);
    }
}

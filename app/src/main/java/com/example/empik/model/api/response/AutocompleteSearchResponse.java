package com.example.empik.model.api.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.arlib.floatingsearchview.suggestions.model.SearchSuggestion;

public class AutocompleteSearchResponse implements Parcelable, SearchSuggestion {
    private int Version;
    private String Key;
    private String Type;
    private int Rank;
    private String LocalizedName;
    private Country Country;
    private AdministrativeArea AdministrativeArea;

    public AutocompleteSearchResponse() {
    }

    public AutocompleteSearchResponse(int version, String key, String type, int rank, String localizedName, AutocompleteSearchResponse.Country country, AutocompleteSearchResponse.AdministrativeArea administrativeArea) {
        Version = version;
        Key = key;
        Type = type;
        Rank = rank;
        LocalizedName = localizedName;
        Country = country;
        AdministrativeArea = administrativeArea;
    }

    protected AutocompleteSearchResponse(Parcel in) {
        Version = in.readInt();
        Key = in.readString();
        Type = in.readString();
        Rank = in.readInt();
        LocalizedName = in.readString();
    }

    public static final Creator<AutocompleteSearchResponse> CREATOR = new Creator<AutocompleteSearchResponse>() {
        @Override
        public AutocompleteSearchResponse createFromParcel(Parcel in) {
            return new AutocompleteSearchResponse(in);
        }

        @Override
        public AutocompleteSearchResponse[] newArray(int size) {
            return new AutocompleteSearchResponse[size];
        }
    };

    public int getVersion() {
        return Version;
    }

    public void setVersion(int version) {
        Version = version;
    }

    public String getKey() {
        return Key;
    }

    public void setKey(String key) {
        Key = key;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public int getRank() {
        return Rank;
    }

    public void setRank(int rank) {
        Rank = rank;
    }

    public String getLocalizedName() {
        return LocalizedName;
    }

    public void setLocalizedName(String localizedName) {
        LocalizedName = localizedName;
    }

    public AutocompleteSearchResponse.Country getCountry() {
        return Country;
    }

    public void setCountry(AutocompleteSearchResponse.Country country) {
        Country = country;
    }

    public AutocompleteSearchResponse.AdministrativeArea getAdministrativeArea() {
        return AdministrativeArea;
    }

    public void setAdministrativeArea(AutocompleteSearchResponse.AdministrativeArea administrativeArea) {
        AdministrativeArea = administrativeArea;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(Version);
        dest.writeString(Key);
        dest.writeString(Type);
        dest.writeInt(Rank);
        dest.writeString(LocalizedName);
    }

    @Override
    public String getBody() {
        if(getLocalizedName() == null || getAdministrativeArea() == null || getCountry() == null)
            return "";

        String body = getLocalizedName() + ", " + getAdministrativeArea().ID + ", " + getCountry().ID;
        return body;
    }

    public class Country {
        private String ID;
        private String LocalizedName;

        public String getID() {
            return ID;
        }

        public void setID(String ID) {
            this.ID = ID;
        }

        public String getLocalizedName() {
            return LocalizedName;
        }

        public void setLocalizedName(String localizedName) {
            LocalizedName = localizedName;
        }

        public Country(String ID, String localizedName) {
            this.ID = ID;
            LocalizedName = localizedName;
        }
    }

    public class AdministrativeArea {
        private String ID;
        private String LocalizedName;

        public AdministrativeArea(String ID, String localizedName) {
            this.ID = ID;
            LocalizedName = localizedName;
        }

        public String getID() {
            return ID;
        }

        public void setID(String ID) {
            this.ID = ID;
        }

        public String getLocalizedName() {
            return LocalizedName;
        }

        public void setLocalizedName(String localizedName) {
            LocalizedName = localizedName;
        }
    }
}

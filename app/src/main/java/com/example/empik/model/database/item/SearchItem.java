package com.example.empik.model.database.item;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class SearchItem {
    @PrimaryKey(autoGenerate = true)
    private long id;
    private String localizedName;
    private String administrativeArea;
    private String country;
    private String localizeKey;

    public String getLocalizeKey() {
        return localizeKey;
    }

    public void setLocalizeKey(String localizeKey) {
        this.localizeKey = localizeKey;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLocalizedName() {
        return localizedName;
    }

    public void setLocalizedName(String localizedName) {
        this.localizedName = localizedName;
    }

    public String getAdministrativeArea() {
        return administrativeArea;
    }

    public void setAdministrativeArea(String administrativeArea) {
        this.administrativeArea = administrativeArea;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}

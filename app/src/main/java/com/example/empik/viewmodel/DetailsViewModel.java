package com.example.empik.viewmodel;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.widget.Toast;

import com.example.empik.R;
import com.example.empik.model.api.response.DailyForecastsResponse;
import com.example.empik.model.repository.DetailRepository;

import io.reactivex.observers.DisposableObserver;

public class DetailsViewModel extends ViewModel {
    private DetailRepository detailRepository;
    private MutableLiveData<DailyForecastsResponse> dailyForecastsResponseMutableLiveData = new MutableLiveData<>();
    public MutableLiveData<String> text = new MutableLiveData<>();
    public MutableLiveData<String> category = new MutableLiveData<>();

    public DetailsViewModel(DetailRepository detailRepository) {
        this.detailRepository = detailRepository;
    }

    public MutableLiveData<DailyForecastsResponse> getDailyForecastsResponseMutableLiveData() {
        return dailyForecastsResponseMutableLiveData;
    }

    public void init(String key) {
        detailRepository.getGetDailyForecastsUC().execute(new DisposableObserver<DailyForecastsResponse>() {
            @Override
            public void onNext(DailyForecastsResponse dailyForecastsResponse) {
                dailyForecastsResponseMutableLiveData.postValue(dailyForecastsResponse);
                text.postValue(dailyForecastsResponse.getHeadline().getText());
                category.postValue(dailyForecastsResponse.getHeadline().getCategory());
            }

            @Override
            public void onError(Throwable e) {
                Toast.makeText(detailRepository.getContext(), detailRepository.getContext().getString(R.string.error_message) + e.getMessage(), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onComplete() {
            }
        }, key);
    }


    @Override
    protected void onCleared() {
        super.onCleared();
        detailRepository.getGetDailyForecastsUC().dispose();
    }
}

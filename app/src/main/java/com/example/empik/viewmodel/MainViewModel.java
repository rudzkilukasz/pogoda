package com.example.empik.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.example.empik.model.database.item.SearchItem;
import com.example.empik.model.repository.MainRepository;

import java.util.List;

public class MainViewModel extends ViewModel {

    private MainRepository mainRepository;
    private LiveData<List<SearchItem>> listLiveDataSearchItem;

    public MainViewModel(MainRepository mainRepository) {
        this.mainRepository = mainRepository;

        init();
    }

    public LiveData<List<SearchItem>> getListLiveDataSearchItem() {
        return listLiveDataSearchItem;
    }

    private void init() {
        listLiveDataSearchItem = mainRepository.getSearchItem();
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        mainRepository.getGetAutocompleteSearchUC().dispose();
    }
}

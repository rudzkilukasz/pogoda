package com.example.empik.dependencyInjection.modules;

import android.arch.persistence.room.Room;
import android.content.Context;

import com.example.empik.dependencyInjection.scope.ApplicationScope;
import com.example.empik.model.database.MyDatabase;

import dagger.Module;
import dagger.Provides;

@Module(includes = ContextModule.class)
public class RoomModule {
    private final MyDatabase database;

    public RoomModule(Context context) {
        this.database = Room.databaseBuilder(
                context,
                MyDatabase.class,
                "MyDatabase.db"
        ).fallbackToDestructiveMigration().build();
    }

    @Provides
    @ApplicationScope
    public MyDatabase myDatabase() {
        return database;
    }
}

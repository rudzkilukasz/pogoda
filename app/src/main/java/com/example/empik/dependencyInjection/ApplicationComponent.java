package com.example.empik.dependencyInjection;

import com.example.empik.dependencyInjection.modules.RoomModule;
import com.example.empik.dependencyInjection.modules.ServiceApiModule;
import com.example.empik.dependencyInjection.scope.ApplicationScope;
import com.example.empik.model.api.ModelClient;
import com.example.empik.model.database.MyDatabase;
import com.example.empik.view.base.BaseApplication;

import dagger.Component;

@ApplicationScope
@Component(modules = {ServiceApiModule.class, RoomModule.class})
public interface ApplicationComponent {
    ModelClient modelClient();
    MyDatabase myDatabase();

    void inject(BaseApplication baseApplication);
}

package com.example.empik.dependencyInjection.modules;

import com.example.empik.dependencyInjection.scope.ApplicationScope;
import com.example.empik.model.api.ModelClient;
import com.example.empik.model.api.ServiceGenerator;

import dagger.Module;
import dagger.Provides;

@Module
public class ServiceApiModule {
    @ApplicationScope
    @Provides
    public ModelClient modelClient() {
        return ServiceGenerator.createService(ModelClient.class);
    }
}

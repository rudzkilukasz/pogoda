package com.example.empik.dependencyInjection;

import com.example.empik.dependencyInjection.modules.BaseActivityModule;
import com.example.empik.dependencyInjection.scope.ActivityScope;
import com.example.empik.view.DetailsActivity;
import com.example.empik.view.MainActivity;

import dagger.Component;

@ActivityScope
@Component(modules = {BaseActivityModule.class}, dependencies = {ApplicationComponent.class})
public interface ActivityComponent {
    void inject(MainActivity mainActivity);
    void inject(DetailsActivity detailsActivity);
}

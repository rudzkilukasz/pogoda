package com.example.empik.utils;

import com.arlib.floatingsearchview.FloatingSearchView;

import java.util.concurrent.TimeUnit;

import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;

public class RxDebounceOnQueryChangeListener implements FloatingSearchView.OnQueryChangeListener {
    private final PublishSubject<String> subject;
    private FloatingSearchView.OnQueryChangeListener onQueryChangeListener;

    public RxDebounceOnQueryChangeListener(long timeOutMilliseconds, FloatingSearchView.OnQueryChangeListener onQueryChangeListener, OnQueryDebounceListener onQueryDebounceListener) {
        this.onQueryChangeListener = onQueryChangeListener;

        subject = PublishSubject.create();
        subject
                .debounce(timeOutMilliseconds, TimeUnit.MILLISECONDS)
                .distinctUntilChanged()
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<String>() {
                    @Override
                    public void accept(String s) throws Exception {
                        onQueryDebounceListener.onSearchTextDebounce(s);
                    }
                });

    }

    @Override
    public void onSearchTextChanged(String oldQuery, String newQuery) {
        onQueryChangeListener.onSearchTextChanged(oldQuery, newQuery);
        subject.onNext(newQuery);
    }

    public interface OnQueryDebounceListener {
        void onSearchTextDebounce(String query);
    }
}

package com.example.empik;

import android.app.Activity;
import android.content.Intent;

import com.example.empik.view.DetailsActivity;

import javax.inject.Inject;

public class Navigator {

    @Inject
    Activity activity;

    @Inject
    public Navigator() {
    }

    public void navigateToDetailActivity(String idKey) {
        Intent intent = new Intent(activity, DetailsActivity.class);
        intent.putExtra(DetailsActivity.DetailsActivityBundle, idKey);
        activity.startActivity(intent);
    }
}
